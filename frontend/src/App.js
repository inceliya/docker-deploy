import logo from './logo.svg';
import './App.css';
import axios from 'axios';

function App() {
  const onButtonClick = () => {
    axios.get("/api/users").then((res) => {
      console.log(res);
    })
  }

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Deployed by Maksym Zakharchuk
        </p>
        <button onClick={onButtonClick}>
          Learn React
        </button>
      </header>
    </div>
  );
}

export default App;
